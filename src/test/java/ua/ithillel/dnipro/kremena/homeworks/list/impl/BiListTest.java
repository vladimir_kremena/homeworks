package ua.ithillel.dnipro.kremena.homeworks.list.impl;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BiListTest {

    @Test
    @DisplayName("Проверка функционала: добавления, получения элемента связанного списка, проверки на пустоту " +
            "и размера списка")
    public void testAddAndGet() {
        List<Integer> list = new BiList<>();
        assertAll(
                ()-> assertTrue(list.isEmpty()),
                ()-> assertTrue(list.add(0)),
                ()-> assertTrue(list.add(1)),
                ()-> assertTrue(list.add(2)),
                ()-> assertTrue(list.add(3)),
                ()-> assertTrue(list.add(4)),
                ()-> assertFalse(list.isEmpty()),
                ()-> assertEquals(5, list.size()),
                ()-> assertEquals(0, list.get(0)),
                ()-> assertEquals(1, list.get(1)),
                ()-> assertEquals(2, list.get(2)),
                ()-> assertEquals(3, list.get(3)),
                ()-> assertEquals(4, list.get(4))
        );
    }

    @Test
    @DisplayName("Проверка функционала: преобразования связанного списка в Object-массив " +
            "и массив типа элементов списка")
    public void testToArray() {
        List<Integer> integerList = new BiList<>();
        integerList.add(0);
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
        integerList.add(4);
        integerList.add(5);

        List<Character> charList = new BiList<>();
        charList.add('A');
        charList.add('B');
        charList.add('C');
        charList.add('D');
        charList.add('E');
        charList.add('F');

        assertAll(
                ()-> assertArrayEquals(new Object[] {0, 1, 2, 3, 4, 5}, integerList.toArray()),
                ()-> assertArrayEquals(new Integer[] {0, 1, 2, 3, 4, 5}, integerList.toArray(new Integer[0])),
                ()-> assertArrayEquals(new Object[] {'A', 'B', 'C', 'D', 'E', 'F'}, charList.toArray()),
                ()-> assertArrayEquals(new Character[] {'A', 'B', 'C', 'D', 'E', 'F'}, charList.toArray(
                        new Character[0])),
                ()-> assertThrows(ArrayStoreException.class, ()-> charList.toArray(new Integer[0]))
        );
    }

    @Test
    @DisplayName("Проверка итератора")
    public void testIterator() {
        List<Character> charList = new BiList<>();
        charList.add('A');
        charList.add('B');
        charList.add('C');
        charList.add('D');
        charList.add('E');
        charList.add('F');

        StringBuilder builder = new StringBuilder();
        for (Character character : charList) {
            builder.append(character);
        }

        assertEquals("ABCDEF", builder.toString());

        Iterator<Character> charIterator = charList.iterator();
        charIterator.remove();
        assertArrayEquals(new Character[] {'B', 'C', 'D', 'E', 'F'}, charList.toArray(new Character[0]));
        charIterator.remove();
        assertArrayEquals(new Character[] {'C', 'D', 'E', 'F'}, charList.toArray(new Character[0]));
        charIterator.next();
        charIterator.remove();
        assertArrayEquals(new Character[] {'C', 'E', 'F'}, charList.toArray(new Character[0]));
        charIterator.next();
        charIterator.remove();
        assertArrayEquals(new Character[] {'C', 'E'}, charList.toArray(new Character[0]));
    }

    @Test
    @DisplayName("Проверка обратного итератора")
    public void testReversIterator() {
        BiList<Character> charList = new BiList<>();
        charList.add('A');
        charList.add('B');
        charList.add('C');
        charList.add('D');
        charList.add('E');
        charList.add('F');

        Iterator<Character> firstIter = charList.reversIterator();
        StringBuilder builder = new StringBuilder();
        while(firstIter.hasNext()) {
            builder.append(firstIter.next());
        }

        Iterator<Character> secondIter = charList.reversIterator();

        assertEquals("FEDCBA", builder.toString());

        secondIter.remove();

        assertArrayEquals(new Character[] {'A', 'B', 'C', 'D', 'E'}, charList.toArray(new Character[0]));
        secondIter.remove();
        assertArrayEquals(new Character[] {'A', 'B', 'C', 'D'}, charList.toArray(new Character[0]));
        secondIter.next();
        secondIter.remove();
        assertArrayEquals(new Character[] {'A', 'B', 'D'}, charList.toArray(new Character[0]));
        secondIter.next();
        secondIter.remove();
        assertArrayEquals(new Character[] {'A', 'D'}, charList.toArray(new Character[0]));
    }

    @Test
    @DisplayName("Проверка функционала: проверка наличия искомого элемента в связанном списке, " +
            "проверка списка на наличие всех элементов принимаемой коллекции")
    public void testContainsAndContainsAll() {
        List<Integer> list = new BiList<>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(5);
        list.add(3);
        list.add(1);
        list.add(8);

        List<Integer> firstIncomingList = new BiList<>();
        firstIncomingList.add(2);
        firstIncomingList.add(3);
        firstIncomingList.add(5);

        List<Integer> secondIncomingList = new BiList<>();
        secondIncomingList.add(1);
        secondIncomingList.add(2);
        secondIncomingList.add(9);

        assertAll(
                ()-> assertTrue(list.contains(0)),
                ()-> assertTrue(list.contains(3)),
                ()-> assertFalse(list.contains(21)),
                ()-> assertTrue(list.contains(8)),
                ()-> assertTrue(list.containsAll(firstIncomingList)),
                ()-> assertFalse(list.containsAll(secondIncomingList))
        );
    }

    @Test
    @DisplayName("Проверка функционала: удаление элемента списка по индексу, по значению и удаление элементов, " +
            "которые присутствуют в принимаемой коллекции")
    public void testRemoveAndRemoveAll() {
        List<Integer> list = new BiList<>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(10);

        List<Integer> incomingList = new BiList<>();
        incomingList.add(1);
        incomingList.add(2);
        incomingList.add(3);
        incomingList.add(4);

        assertAll(
                ()-> assertEquals(11, list.size()),
                ()-> assertEquals(0, list.remove(0)),
                ()-> assertEquals(10, list.size()),
                ()-> assertArrayEquals(new Integer[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, list.toArray(new Integer[0])),
                ()-> assertTrue(list.remove(Integer.valueOf(5))),
                ()-> assertEquals(9, list.size()),
                ()-> assertArrayEquals(new Integer[] {1, 2, 3, 4, 6, 7, 8, 9, 10}, list.toArray(new Integer[0])),
                ()-> assertTrue(list.removeAll(incomingList)),
                ()-> assertArrayEquals(new Integer[] {6, 7, 8, 9, 10}, list.toArray(new Integer[0]))
        );
    }

    @Test
    @DisplayName("Проверка функционала: добавление коллекции в конец списка, добавление коллекции начиная с указанной" +
            " позиции списка")
    public void testAddAll() {
        List<Integer> list = new BiList<>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);

        List<Integer> firstAddedList = new BiList<>();
        firstAddedList.add(5);
        firstAddedList.add(6);
        firstAddedList.add(7);
        firstAddedList.add(8);
        firstAddedList.add(9);

        List<Integer> secondAddedList = new BiList<>();
        secondAddedList.add(10);
        secondAddedList.add(11);
        secondAddedList.add(12);

        List<Integer> thirdAddedList = new BiList<>();
        thirdAddedList.add(3);
        thirdAddedList.add(3);
        thirdAddedList.add(3);

        assertAll(
                ()-> assertTrue(list.addAll(firstAddedList)),
                ()-> assertArrayEquals(new Integer[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, list.toArray(new Integer[0])),
                ()-> assertEquals(10, list.size()),
                ()-> assertTrue(list.addAll(9, secondAddedList)),
                ()-> assertEquals(13, list.size()),
                ()-> assertArrayEquals(new Integer[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 9}, list.toArray(new Integer[0])),
                ()-> assertTrue(list.addAll(3, thirdAddedList)),
                ()-> assertArrayEquals(new Integer[] {0, 1, 2, 3, 3, 3, 3, 4, 5, 6, 7, 8, 10, 11, 12, 9},
                        list.toArray(new Integer[0]))
        );
    }

    @Test
    @DisplayName("Проверка функционала: очистка связанного списка")
    public void testClear() {
        List<Integer> list = new BiList<>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);

        assertFalse(list.isEmpty());
        assertEquals(5, list.size());
        list.clear();
        assertTrue(list.isEmpty());
        assertEquals(0, list.size());
    }

    @Test
    @DisplayName("Проверка функционала: выделение подсписка из исходного списка")
    public void testSubList() {
        List<Character> charList = new BiList<>();
        charList.add('A');
        charList.add('B');
        charList.add('C');
        charList.add('D');
        charList.add('E');
        charList.add('F');
        assertArrayEquals(new Character[] {'A', 'B', 'C'}, charList.subList(0, 3).toArray(new Character[0]));
    }

    @Test
    @DisplayName("Проверка функционала: удаление элементов списка, отсутствующих во входящей коллекции")
    public void testRetainAll() {
        List<Integer> list = new BiList<>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);

        List<Integer> incomingList = new BiList<>();
        incomingList.add(1);
        incomingList.add(2);
        incomingList.add(3);
        incomingList.add(4);

        assertAll(
                ()-> assertEquals(10, list.size()),
                ()-> assertTrue(list.retainAll(incomingList)),
                ()-> assertEquals(4, list.size()),
                ()-> assertArrayEquals(new Integer[] {1, 2, 3, 4}, list.toArray(new Integer[0]))
        );
    }

    @Test
    @DisplayName("Проверка функционала: замена элемента списка по индексу")
    public void testSet() {
        List<Character> charList = new BiList<>();
        charList.add('A');
        charList.add('B');
        charList.add('C');
        charList.add('D');

        assertAll(
                ()-> assertEquals(4, charList.size()),
                ()-> assertEquals('B', charList.set(1, 'Z')),
                ()-> assertEquals('Z', charList.get(1)),
                ()-> assertThrows(IndexOutOfBoundsException.class, ()-> charList.set(10, 'H')),
                ()-> assertEquals(4, charList.size())
        );
    }

    @Test
    @DisplayName("Проверка функционала: добавление элемента списка по индексу")
    public void testAdd() {
        List<Integer> list = new BiList<>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);

        assertEquals(4, list.size());
        list.add(0, 0);
        assertEquals(5, list.size());
        assertArrayEquals(new Integer[] {0, 0, 1, 2, 3}, list.toArray(new Integer[0]));
        list.add(4, 4);
        assertEquals(6, list.size());
        assertArrayEquals(new Integer[] {0, 0, 1, 2, 4, 3}, list.toArray(new Integer[0]));
        list.add(2, 101);
        assertEquals(7, list.size());
        assertArrayEquals(new Integer[] {0, 0, 101, 1, 2, 4, 3}, list.toArray(new Integer[0]));
    }

    @Test
    @DisplayName("Проверка функционала: получение индекса первого и последнего вхождения элемента списка")
    public void testIndexOfAndLastIndexOf() {
        List<Integer> list = new BiList<>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(4);
        list.add(3);
        list.add(2);
        list.add(1);
        list.add(0);

        assertAll(
                ()-> assertEquals(2, list.indexOf(2)),
                ()-> assertEquals(8, list.lastIndexOf(2))
        );
    }
}