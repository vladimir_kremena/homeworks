package ua.ithillel.dnipro.kremena.homeworks.entity;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CircleTest {
    @Test
    @DisplayName("Тест функционала проверки нахождения точки в пределах круга")
    public void testContainsPoint() {
        Circle circle = new Circle(new Point(0, 0), 5);

        Point pointFirst = new Point(0, 0);
        Point pointSecond = new Point(4, 0);
        Point pointThird = new Point(4, 3);
        Point pointFourth = new Point(0, 5);

        Point pointFifth = new Point(5.1, 5.1);
        Point pointSixth = new Point(100, 100);

        assertAll(
                ()-> assertTrue(circle.containsPoint(pointFirst)),
                ()-> assertTrue(circle.containsPoint(pointSecond)),
                ()-> assertTrue(circle.containsPoint(pointThird)),
                ()-> assertTrue(circle.containsPoint(pointFourth)),
                ()-> assertFalse(circle.containsPoint(pointFifth)),
                ()-> assertFalse(circle.containsPoint(pointSixth))
        );
    }
}