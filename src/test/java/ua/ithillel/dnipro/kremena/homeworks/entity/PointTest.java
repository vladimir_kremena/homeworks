package ua.ithillel.dnipro.kremena.homeworks.entity;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PointTest {
    @Test
    @DisplayName("Тест функционала рассчета расстояния между точками")
    public void testDistanceToPoint() {

        Point pointFirst = new Point(0, 0);
        Point pointSecond = new Point(4, 0);
        Point pointThird = new Point(4, 3);
        Point pointNull = null;

        assertAll(
                ()->assertEquals(0.0, pointFirst.distanceToPoint(pointFirst)),
                ()->assertEquals(4.0, pointFirst.distanceToPoint(pointSecond)),
                ()->assertEquals(5.0, pointFirst.distanceToPoint(pointThird)),
                ()->assertThrows(IllegalArgumentException.class, ()-> pointFirst.distanceToPoint(pointNull)),
                ()->assertTrue(pointFirst.equals(pointFirst)),
                ()->assertFalse(pointFirst.equals(pointThird))
        );
    }
}