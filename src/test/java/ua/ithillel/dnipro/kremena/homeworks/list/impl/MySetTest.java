package ua.ithillel.dnipro.kremena.homeworks.list.impl;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ua.ithillel.dnipro.kremena.homeworks.entity.Human;

import java.util.Iterator;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class MySetTest {

    @Test
    @DisplayName("boolean add(E e) ,  boolean isEmpty() , void clear() , int size() | " +
            "Проверка функционала: добавления элемента набора, проверки на пустоту, очистка и размера списка")
    public void testAddAndIsEmptyAndSizeAndClear() {

        Set<Human> set = new MySet<>();

        Human human00 = new Human(-1, 1);
        Human human01 = new Human(-2, 2);
        Human human02 = new Human(-3, 3);

        Human human10 = new Human(-4, 5);
        Human human11 = new Human(-5, 6);

        Human human20 = new Human(-6, 8);
        Human human21 = new Human(-7, 9);
        Human human22 = new Human(-8, 10);

        assertAll(
                ()-> assertEquals(0, set.size()),
                ()-> assertTrue(set.isEmpty())
        );

        set.add(human00);

        assertAll(
                ()-> assertEquals(1, set.size()),
                ()-> assertFalse(set.isEmpty())
        );

        set.add(human01);
        set.add(human02);
        set.add(human10);
        set.add(human11);
        set.add(human20);
        set.add(human21);
        set.add(human22);

        assertAll(
                ()-> assertEquals(8, set.size()),
                ()-> assertFalse(set.isEmpty())
        );

        set.add(human01);
        set.add(human02);
        set.add(human10);

        assertAll(
                ()-> assertEquals(8, set.size()),
                ()-> assertFalse(set.isEmpty())
        );

        set.clear();

        assertAll(
                ()-> assertEquals(0, set.size()),
                ()-> assertTrue(set.isEmpty())
        );
    }

    @Test
    @DisplayName("Object[] toArray() , T[] toArray(T[] a) | Проверка функционала: преобразования набора " +
            "в Object-массив и массив типа элементов списка")
    public void testToArray() {
        Set<Human> set = new MySet<>();

        Human human00 = new Human(-1, 1);
        Human human10 = new Human(-4, 5);
        Human human11 = new Human(-5, 6);
        Human human20 = new Human(-6, 8);
        Human human21 = new Human(-7, 9);

        set.add(human00);
        set.add(human10);
        set.add(human11);
        set.add(human20);
        set.add(human21);

        assertAll(
                ()-> assertArrayEquals(new Human[] {
                        new Human(-1, 1),
                        new Human(-4, 5),
                        new Human(-5, 6),
                        new Human(-6, 8),
                        new Human(-7, 9)
                }, set.toArray()),
                ()-> assertArrayEquals(new Human[] {
                        new Human(-1, 1),
                        new Human(-4, 5),
                        new Human(-5, 6),
                        new Human(-6, 8),
                        new Human(-7, 9)
                }, set.toArray(new Human[0]))
        );
    }

    @Test
    @DisplayName("Iterator<E> iterator() | Проверка итератора")
    public void testIterator() {

        Set<Human> set = new MySet<>();

        Human human00 = new Human(-1, 1);
        Human human01 = new Human(-2, 2);
        Human human02 = new Human(-3, 3);
        Human human03 = new Human(-4, 4);
        Human human04 = new Human(-5, 5);

        Human human10 = new Human(-6, 7);
        Human human11 = new Human(-7, 8);
        Human human12 = new Human(-8, 9);
        Human human13 = new Human(-9, 10);
        Human human14 = new Human(-10, 11);

        Human human20 = new Human(-11, 13);
        Human human21 = new Human(-12, 14);
        Human human22 = new Human(-13, 15);
        Human human23 = new Human(-14, 16);
        Human human24 = new Human(-15, 17);

        Human human40 = new Human(-16, 21);
        Human human41 = new Human(-17, 22);
        Human human42 = new Human(-18, 23);
        Human human43 = new Human(-19, 24);
        Human human44 = new Human(-20, 25);

        set.add(human00);
        set.add(human01);
        set.add(human02);
        set.add(human03);
        set.add(human04);
        set.add(human10);
        set.add(human11);
        set.add(human12);
        set.add(human13);
        set.add(human14);
        set.add(human20);
        set.add(human21);
        set.add(human22);
        set.add(human23);
        set.add(human24);
        set.add(human40);
        set.add(human41);
        set.add(human42);
        set.add(human43);
        set.add(human44);

        Human[] humans = new Human[] {human00, human01, human02, human03, human04, human10, human11, human12, human13,
                human14, human20, human21, human22, human23, human24, human40, human41, human42, human43, human44};
        int index = 0;

        for (Human human : set) {
            assertEquals(human, humans[index]);
            index++;
        }

        Iterator<Human> iterator = set.iterator();

        if (iterator.hasNext()) {
            assertEquals(iterator.next(), human00);
        }
    }

    @Test
    @DisplayName("contains(Object o) , containsAll(Collection<?> c) | проверка наличия искомого элемента в наборе, " +
            "проверка набора на наличие всех элементов принимаемой коллекции")
    public void testContainsAndContainsAll() {

        Set<Integer> set = new MySet<>();
        set.add(10);
        set.add(15);
        set.add(20);
        set.add(25);
        set.add(30);

        assertAll(
                ()-> assertTrue(set.contains(25)),
                ()-> assertFalse(set.contains(100))
        );

        Set<Integer> firstIncomingSet = new MySet<>();
        firstIncomingSet.add(15);
        firstIncomingSet.add(20);
        firstIncomingSet.add(25);

        Set<Integer> secondIncomingSet = new MySet<>();
        secondIncomingSet.add(15);
        secondIncomingSet.add(100);
        secondIncomingSet.add(25);

        assertAll(
                ()-> assertTrue(set.containsAll(firstIncomingSet)),
                ()-> assertFalse(set.containsAll(secondIncomingSet))
        );
    }

    @Test
    @DisplayName("boolean addAll(Collection<? extends E> c) | Проверка функционала: добавление коллекции в конец набора")
    public void testAddAll() {
        Set<Integer> set = new MySet<>();
        set.add(10);
        set.add(15);
        set.add(20);
        set.add(25);
        set.add(30);

        assertEquals(5, set.size());

        Set<Integer> firstIncomingSet = new MySet<>();
        firstIncomingSet.add(15);
        firstIncomingSet.add(20);
        firstIncomingSet.add(25);

        set.addAll(firstIncomingSet);
        assertEquals(5, set.size());

        Set<Integer> secondIncomingSet = new MySet<>();
        secondIncomingSet.add(50);
        secondIncomingSet.add(60);
        secondIncomingSet.add(70);

        set.addAll(secondIncomingSet);
        assertEquals(8, set.size());
    }

    @Test
    @DisplayName("boolean remove(Object o) , boolean removeAll(Collection<?> c) | " +
            "Проверка функционала: удаление элемента набора, удаление элементов набора, " +
            "которые присутствуют в принимаемой коллекции")
    public void testRemoveAndRemoveAll() {
        Set<Human> set = new MySet<>(8);

        Human human00 = new Human(-1, 1);
        Human human01 = new Human(-2, 2);
        Human human02 = new Human(-3, 3);
        Human human03 = new Human(-4, 4);
        Human human04 = new Human(-5, 5);

        Human human10 = new Human(-6, 7);
        Human human11 = new Human(-7, 8);
        Human human12 = new Human(-8, 9);
        Human human13 = new Human(-9, 10);
        Human human14 = new Human(-10, 11);

        Human human20 = new Human(-11, 13);
        Human human21 = new Human(-12, 14);
        Human human22 = new Human(-13, 15);
        Human human23 = new Human(-14, 16);
        Human human24 = new Human(-15, 17);

        Human human40 = new Human(-16, 21);
        Human human41 = new Human(-17, 22);
        Human human42 = new Human(-18, 23);
        Human human43 = new Human(-19, 24);
        Human human44 = new Human(-20, 25);

        set.add(human00);
        set.add(human01);
        set.add(human02);
        set.add(human03);
        set.add(human04);
        set.add(human10);
        set.add(human11);
        set.add(human12);
        set.add(human13);
        set.add(human14);
        set.add(human20);
        set.add(human21);
        set.add(human22);
        set.add(human23);
        set.add(human24);
        set.add(human40);
        set.add(human41);
        set.add(human42);
        set.add(human43);
        set.add(human44);

        assertEquals(20, set.size());
        assertTrue(set.contains(human00));
        assertTrue(set.contains(human02));
        assertTrue(set.contains(human04));
        assertTrue(set.contains(human20));
        assertTrue(set.contains(human44));

        assertTrue(set.remove(human00));
        assertTrue(set.remove(human02));
        assertTrue(set.remove(human04));
        assertTrue(set.remove(human20));
        assertTrue(set.remove(human44));

        assertFalse(set.remove(human00));
        assertFalse(set.remove(human02));
        assertFalse(set.remove(human04));
        assertFalse(set.remove(human20));
        assertFalse(set.remove(human44));

        assertEquals(15, set.size());

        Set<Human> incomingSet = new MySet<>();

        assertTrue(set.contains(human23));
        assertTrue(set.contains(human24));
        assertTrue(set.contains(human40));
        assertTrue(set.contains(human41));
        assertTrue(set.contains(human42));

        incomingSet.add(human23);
        incomingSet.add(human24);
        incomingSet.add(human40);
        incomingSet.add(human41);
        incomingSet.add(human42);
        Human human45 = new Human(-21, 26);
        incomingSet.add(human45);

        set.removeAll(incomingSet);

        assertFalse(set.contains(human23));
        assertFalse(set.contains(human24));
        assertFalse(set.contains(human40));
        assertFalse(set.contains(human41));
        assertFalse(set.contains(human42));
        assertEquals(10, set.size());
    }

    @Test
    @DisplayName("boolean retainAll(Collection<?> c) | Проверка функционала: удаление элементов набора, " +
            "отсутствующих во входящей коллекции")
    public void testRetainAll() {

        Set<Integer> set = new MySet<>();
        set.add(10);
        set.add(15);
        set.add(20);
        set.add(25);
        set.add(30);
        set.add(35);
        set.add(40);
        set.add(45);

        assertEquals(8, set.size());

        Set<Integer> incomingSet = new MySet<>();
        incomingSet.add(20);
        incomingSet.add(25);
        incomingSet.add(30);
        incomingSet.add(35);

        set.retainAll(incomingSet);

        assertEquals(4, set.size());

        assertArrayEquals(set.toArray(new Integer[0]), incomingSet.toArray(new Integer[0]));
    }
}