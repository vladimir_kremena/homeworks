package ua.ithillel.dnipro.kremena.homeworks.list.impl;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ua.ithillel.dnipro.kremena.homeworks.list.ObjectList;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

public class SimpleObjectListTest {

    @Test
    @DisplayName("Тест функционала добавления элемента коллекции")
    public void testAdd() {
        ObjectList<Integer> list = new SimpleObjectList<>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        assertAll(
                ()-> assertEquals(5, list.size()),
                ()-> assertEquals(0, list.get(0)),
                ()-> assertEquals(4, list.get(4))
        );
    }

    @Test
    @DisplayName("Тест функционала добавления коллекции в коллекцию")
    public void testAddAll() {
        ObjectList<Integer> firstList = new SimpleObjectList<>();
        firstList.add(0);
        firstList.add(1);
        firstList.add(2);

        ObjectList<Integer> secondList = new SimpleObjectList<>();
        secondList.add(3);
        secondList.add(4);
        firstList.addAll(secondList);

        assertAll(
                ()-> assertEquals(5, firstList.size()),
                ()-> assertEquals(3, firstList.get(3)),
                ()-> assertEquals(4, firstList.get(4)),
                ()-> assertThrows(IndexOutOfBoundsException.class, ()->firstList.get(-1)),
                ()-> assertThrows(IndexOutOfBoundsException.class, ()->firstList.get(5))
        );
    }

    @Test
    @DisplayName("Тест функционала перезаписи элемента коллекции по индексу")
    public void testSet() {
        ObjectList<Integer> list = new SimpleObjectList<>();
        list.add(0);
        list.add(1);
        list.add(2);

        list.set(1, 3);

        assertAll(
                ()-> assertEquals(3, list.size()),
                ()-> assertEquals(0, list.get(0)),
                ()-> assertEquals(3, list.get(1)),
                ()-> assertEquals(2, list.get(2))
        );
    }

    @Test
    @DisplayName("Тест выхода за границы коллекции")
    public void testOutOfBounds() {
        ObjectList<Integer> list = new SimpleObjectList<>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);

        assertAll(
                ()-> assertThrows(IndexOutOfBoundsException.class, ()->list.get(-1)),
                ()-> assertThrows(IndexOutOfBoundsException.class, ()->list.get(5)),
                ()-> assertThrows(IndexOutOfBoundsException.class, ()->list.get(21)),

                ()-> assertDoesNotThrow(()->list.get(0)),
                ()-> assertDoesNotThrow(()->list.get(4))
        );
    }

    @Test
    @DisplayName("Тест прямого итератора")
    public void testIterator() {

        ObjectList<String> stringList = new SimpleObjectList<>();
        stringList.add("1-0");
        stringList.add("1-1");
        stringList.add("1-2");

        Iterator<String> stringListIterator = stringList.iterator();

        int indexForEach = 0;

        for (String str : stringList) {
            assertTrue(str.equals(stringList.get(indexForEach++)));
        }

        int indexWhile = 0;
        while(stringListIterator.hasNext()) {
            assertTrue(stringListIterator.next().equals(stringList.get(indexWhile++)));
        }

        assertThrows(IndexOutOfBoundsException.class, ()-> stringListIterator.next());

        ObjectList<Integer> integerList = new SimpleObjectList<>();
        integerList.add(0);
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
        integerList.add(4);
        Iterator<Integer> integerListIterator = integerList.iterator();

        indexForEach = 0;
        for (Integer integer : integerList) {
            assertTrue(integer == integerList.get(indexForEach++));
        }

        indexWhile = 0;
        while(integerListIterator.hasNext()) {
            assertTrue(integerListIterator.next() == integerList.get(indexWhile++));
        }

        assertThrows(IndexOutOfBoundsException.class, ()-> integerListIterator.next());
    }

    @Test
    @DisplayName("Тест реверсивного итератора. Внутренний класс")
    public void testReversIteratorInnerClass() {
        ObjectList<Integer> integerList = new SimpleObjectList<>();
        integerList.add(0);
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
        integerList.add(4);

        Iterator<Integer> integerIterator = integerList.reversIteratorInnerClass();

        int lastIndex = integerList.size() - 1;

        while (integerIterator.hasNext()) {
            assertTrue(integerIterator.next() == integerList.get(lastIndex--));
        }

        assertThrows(IndexOutOfBoundsException.class, ()-> integerIterator.next());
    }

    @Test
    @DisplayName("Тест реверсивного итератора. Анонимный внутренний класс")
    public void testReversIteratorAnonymousClass() {
        ObjectList<Integer> integerList = new SimpleObjectList<>();
        integerList.add(0);
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
        integerList.add(4);

        Iterator<Integer> integerIterator = integerList.reversIteratorAnonymousClass();

        int lastIndex = integerList.size() - 1;

        while (integerIterator.hasNext()) {
            assertTrue(integerIterator.next() == integerList.get(lastIndex--));
        }

        assertThrows(IndexOutOfBoundsException.class, ()-> integerIterator.next());
    }

    @Test
    @DisplayName("Тест скорости добавления элементов")
    public void testSpeedAdd() {
        ObjectList<Integer> integerList = new SimpleObjectList<>();
        for (int i = 0; i < 10_000; i++) {
            integerList.add(i);
        }
    }

    @Test
    @DisplayName("Тест функционала проверки всех элементов коллекции на соответствие предикату")
    public void testEvery() {
        ObjectList<Integer> integerList = new SimpleObjectList<>();
        integerList.add(0);
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
        integerList.add(4);

        ObjectList<Boolean> booleanListAllTrue = new SimpleObjectList<>();
        booleanListAllTrue.add(true);
        booleanListAllTrue.add(true);
        booleanListAllTrue.add(true);
        booleanListAllTrue.add(true);

        ObjectList<Boolean> booleanList = new SimpleObjectList<>();
        booleanList.add(true);
        booleanList.add(false);
        booleanList.add(true);
        booleanList.add(false);

        assertAll(
                ()-> assertTrue(integerList.every(integer -> integer >= 0)),
                ()-> assertTrue(integerList.every(integer -> integer < 5)),
                ()-> assertFalse(integerList.every(integer -> integer == 2)),

                ()-> assertTrue(booleanListAllTrue.every(bool -> bool == true)),
                ()-> assertFalse(booleanListAllTrue.every(bool -> bool == false)),

                ()->assertFalse(booleanList.every(bool -> bool == true)),
                ()->assertFalse(booleanList.every(bool -> bool == false))
        );
    }

    @Test
    @DisplayName("Тест функционала проверки коллекции на соответствие хоть одного элемента предикату")
    public void testSome() {
        ObjectList<Integer> integerList = new SimpleObjectList<>();
        integerList.add(0);
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
        integerList.add(4);

        ObjectList<Character> charList = new SimpleObjectList<>();
        charList.add('a');
        charList.add('b');
        charList.add('c');
        charList.add('d');
        charList.add('e');

        assertAll(
                ()-> assertTrue(integerList.some(integer -> integer == 2)),
                ()-> assertFalse(integerList.some(integer -> integer == 20)),
                ()-> assertTrue(integerList.some(integer -> integer <= 0)),
                ()-> assertTrue(integerList.some(integer -> integer >= 4)),

                ()-> assertTrue(charList.some(character -> character == 'c')),
                ()-> assertFalse(charList.some(character -> character == 'f'))
        );
    }

    @Test
    @DisplayName("Тест функционала агрегации элементов коллекции по условию")
    public void testReduce() {
        ObjectList<Integer> integerList = new SimpleObjectList<>();
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
        integerList.add(4);
        integerList.add(5);

        ObjectList<String> stringList = new SimpleObjectList<>();
        stringList.add("A");
        stringList.add("B");
        stringList.add("C");

        assertAll(
                ()-> assertEquals(15, integerList.reduce(0, (arg1, arg2) -> arg2 + arg1)),
                ()-> assertEquals(120, integerList.reduce(1, (arg1, arg2) -> arg2 * arg1)),
                ()-> assertEquals("ABC", stringList.reduce("", (arg1, arg2) -> arg2.concat(arg1)))
        );
    }
}