package ua.ithillel.dnipro.kremena.homeworks.list.impl;

import ua.ithillel.dnipro.kremena.homeworks.list.ObjectList;

import java.util.Arrays;
import java.util.Iterator;
import java.util.function.BiFunction;
import java.util.function.Predicate;

public class SimpleObjectList<T> implements ObjectList<T> {

    private Object[] objects = new Object[0];

    @Override
    public void add(T element) {
        Object[] buffer = new Object[objects.length + 1];
        for (int index = 0; index < objects.length; index++) {
            buffer[index] = objects[index];
        }
        buffer[buffer.length - 1] = element;
        objects = buffer;
    }

    @Override
    public void addAll(ObjectList<T> list) {
        Object[] buffer = new Object[objects.length + list.size()];
        for (int indexOrigin = 0; indexOrigin < objects.length; indexOrigin++) {
            buffer[indexOrigin] = objects[indexOrigin];
        }
        for (int indexInput = 0; indexInput < list.size(); indexInput++) {
            buffer[objects.length + indexInput] = list.get(indexInput);
        }
        objects = buffer;
    }

    @Override
    public T get(int index) {
        return (T) objects[index];
    }

    @Override
    public void set(int index, T element) {
        objects[index] = element;
    }

    @Override
    public int size() {
        return objects.length;
    }

    @Override
    public String toString() {
        return  "SimpleObjectList [" +
                "objects = " + Arrays.toString(objects) +
                ']';
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            private int index;

            @Override
            public boolean hasNext() {
                return index < objects.length;
            }

            @Override
            public T next() {
                return (T)objects[index++];
            }
        };
    }

    @Override
    public Iterator<T> reversIteratorInnerClass() {
        return new IteratorInnerClass<T>();
    }

    @Override
    public Iterator<T> reversIteratorAnonymousClass() {
        return new Iterator<T>() {

            private int index = objects.length - 1;

            @Override
            public boolean hasNext() {
                return index >= 0;
            }

            @Override
            public T next() {
                return (T)objects[index--];
            }
        };
    }

    private class IteratorInnerClass<T> implements Iterator<T> {

        private int index = objects.length - 1;

        @Override
        public boolean hasNext() {
            return index >= 0;
        }

        @Override
        public T next() {
            return (T)objects[index--];
        }
    }
}