package ua.ithillel.dnipro.kremena.homeworks.main;

import ua.ithillel.dnipro.kremena.homeworks.view.UserConsole;

public class Main {
    public static void main(String[] args) {
        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("Homework 5 | begin");
        UserConsole.userDialog();
        System.out.println("Homework 5 | end");
        System.out.println("--------------------------------------------------------------------------------");
    }
}