package ua.ithillel.dnipro.kremena.homeworks.view.menu.imp;

import ua.ithillel.dnipro.kremena.homeworks.entity.Point;
import ua.ithillel.dnipro.kremena.homeworks.services.PointService;
import ua.ithillel.dnipro.kremena.homeworks.view.menu.MenuItem;

import java.util.Scanner;

public class AddPointMenuItem implements MenuItem {

    private Scanner scanner;
    private PointService pointService;

    public AddPointMenuItem(Scanner scanner, PointService pointService) {
        this.scanner = scanner;
        this.pointService = pointService;
    }

    @Override
    public String getName() {
        return "Добавить точку";
    }

    @Override
    public void execute() {
        pointService.addPoint(createPoint());
    }

    private Point createPoint() {
        System.out.println("Введите координаты точки : ");
        System.out.print("Координата x = ");
        double xCoordinate = (Double.parseDouble(scanner.next()));
        System.out.print("Координата y = ");
        double yCoordinate = (Double.parseDouble(scanner.next()));
        return new Point(xCoordinate, yCoordinate);
    }
}