package ua.ithillel.dnipro.kremena.homeworks.list.impl;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.*;

public class BiList<E> implements List<E> {

    private int length;
    private Node<E> head;
    private Node<E> tail;

    @Override
    public int size() {
        return length;
    }

    @Override
    public boolean isEmpty() {
        return length == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (E e : this) {
            if (Objects.equals(o, e)) return true;
        }
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            Node<E> curr = head;
            @Override
            public boolean hasNext() {
                return curr != null;
            }
            @Override
            public E next() {
                E value = curr.getValue();
                curr = curr.getNext();
                return value;
            }
            @Override
            public void remove() {
                curr = removeNode(curr);
            }
        };
    }

    public Iterator<E> reversIterator() {
        return new Iterator<E>() {
            Node<E> curr = tail;
            @Override
            public boolean hasNext() {
                return curr != null;
            }
            @Override
            public E next() {
                E value = curr.getValue();
                curr = curr.getPrev();
                return value;
            }
            @Override
            public void remove() {
                curr = removeNode(curr);
            }
        };
    }

    @Override
    public Object[] toArray() {
        return toArray(new Object[0]);
    }

    @Override
    public <T> T[] toArray(T[] a) {
        T[] array = Arrays.copyOf(a, length);
        int index = 0;
        for (E e : this) {
            array[index++] = (T)e;
        }
        return array;
    }

    @Override
    public boolean add(E e) {
        if (head == null) {
            head = tail = new Node<E>().setValue(e);
        } else {
            Node<E> node = new Node<E>().setValue(e).setPrev(tail);
            tail.setNext(node);
            tail = node;
        }
        length++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        for (Node<E> curr = head; curr != null; curr = curr.getNext()) {
            if(Objects.equals(o, curr.getValue())) {
                removeNode(curr);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if(!contains(o)) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        boolean changed = false;
        for (E e : c) {
            if (add(e)) changed = true;
        }
        return changed;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {

        checkIndex(index);
        if (c.size() == 0) return false;
        boolean changed = false;
        Node<E> prev = head;
        Node<E> next = head.getNext();
        for (int i = 0; i < index -1; i++) {
            prev = prev.getNext();
            next = next.getNext();
        }

        Object[] objects = c.toArray(new Object[0]);

        for (Object object : objects) {
            Node<E> node = new Node<>();
            node.setValue((E)object);
            node.setPrev(prev).setNext(next);
            prev.setNext(node);
            prev = prev.getNext();
            length++;
            changed = true;
        }
        return changed;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean changed = false;
        Iterator<E> iter = iterator();
        for (E e : this) {
            if (c.contains(e)) {
                remove(e);
                changed = true;
            }
        }
        return changed;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean changed = false;
        Iterator<E> iter = iterator();
        for (E e : this) {
            if (!c.contains(e)) {
                remove(e);
                changed = true;
            }
        }
        return changed;
    }

    @Override
    public void clear() {
        head = tail = null;
        length = 0;
    }

    @Override
    public E get(int index) {
        checkIndex(index);
        Node<E> curr = head;
        for (int i = 0; i < index; i++) {
            curr = curr.getNext();
        }
        return curr.getValue();
    }

    @Override
    public E set(int index, E element) {
        checkIndex(index);
        Node<E> curr = head;
        for (int i = 0; i < index; i++) {
            curr = curr.getNext();
        }
        E value = curr.getValue();
        curr.setValue(element);
        return value;
    }

    @Override
    public void add(int index, E element) {
        checkIndex(index);
        Node<E> prev = head;
        for(int i = 0; i < index - 1; i++) {
            prev = prev.getNext();
        }
        prev.setNext(new Node<E>().setValue(element).setPrev(prev).setNext(prev.getNext()));
        length++;
    }

    @Override
    public E remove(int index) {
        checkIndex(index);
        Node<E> curr = head;
        for(int i = 0; i < index; i++) {
            curr = curr.getNext();
        }
        E value = curr.getValue();
        removeNode(curr);
        return value;
    }

    @Override
    public int indexOf(Object o) {
        int index = 0;
        for (Node<E> curr = head; curr != null; curr = curr.getNext()) {
            if(Objects.equals(o, curr.getValue())) {
                return index;
            }
            index++;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        int index = length;
        for (Node<E> curr = tail; curr != null; curr = curr.getPrev()) {
            index--;
            if(Objects.equals(o, curr.getValue())) {
                return index;
            }
        }
        return -1;
    }

    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        checkIndex(fromIndex, toIndex);
        List<E> list = new BiList<>();
        if (fromIndex == toIndex) return list;
        int currIndex = 0;
        for (Node<E> curr = head; curr != null; curr = curr.getNext()) {
            if (currIndex >= fromIndex && currIndex < toIndex) {
                list.add(curr.getValue());
            }
            currIndex++;
        }
        return list;
    }

    private void checkIndex(int index) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException("Элемент с индексом " + index
                + " не существует");
    }

    private void checkIndex(int fromIndex, int toIndex) {
        if (fromIndex < 0) {
            throw new IndexOutOfBoundsException("Элемент с индексом " + fromIndex + " не существует");
        } else if (toIndex >= length)
            throw new IndexOutOfBoundsException("Элемент с индексом " + toIndex + " не существует");
        else if (fromIndex > toIndex) {
            throw new IndexOutOfBoundsException("Хвостовой индекс не может быть меньше головного индекса");
        }
    }

    private Node<E> removeNode(Node<E> curr) {
        if(curr == head && curr == tail) {
            head = tail = null;
            length = 0;
        } else if (curr == head) {
            head.getNext().setPrev(null);
            curr = head.getNext();
            head = head.getNext();
            length--;
        } else if (curr == tail) {
            curr = tail.getPrev();
            tail.getPrev().setNext(null);
            tail = tail.getPrev();
            length--;
        } else {
            curr.getPrev().setNext(curr.getNext());
            curr.getNext().setPrev(curr.getPrev());
            curr = curr.getNext();
            length--;
        }
        return curr;
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    private static class Node<T> {
        private T value;
        private Node<T> next;
        private Node<T> prev;
    }
}