package ua.ithillel.dnipro.kremena.homeworks.entity;

public class Human {

    private Integer id;
    private Integer age;

    public Human(Integer id, Integer age) {
        this.id = id;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return  id.equals(human.id) &&
                age.equals(human.age);
    }

    @Override
    public int hashCode() {
        return id + age;
    }

    @Override
    public String toString() {
        return  "Human [" +
                "id = " + id + " | " +
                "age = " + age + "]";
    }
}