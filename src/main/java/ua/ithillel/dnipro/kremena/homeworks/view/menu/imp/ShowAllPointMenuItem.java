package ua.ithillel.dnipro.kremena.homeworks.view.menu.imp;

import ua.ithillel.dnipro.kremena.homeworks.services.PointService;
import ua.ithillel.dnipro.kremena.homeworks.view.menu.MenuItem;

public class ShowAllPointMenuItem implements MenuItem {

    private PointService pointService;

    public ShowAllPointMenuItem(PointService pointService) {
        this.pointService = pointService;
    }

    @Override
    public String getName() {
        return "Показать все точки";
    }

    @Override
    public void execute() {
        if (pointService.getAll().size() == 0) {
            System.out.println("Еще не добавлено ни одной точки");
        } else {
            for (int index = 0; index < pointService.getAll().size(); index++) {
                System.out.printf("%2d - %s\n", index + 1, pointService.getPoint(index));
            }
        }
    }
}