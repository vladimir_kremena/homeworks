package ua.ithillel.dnipro.kremena.homeworks.services.imp;

import ua.ithillel.dnipro.kremena.homeworks.entity.Point;
import ua.ithillel.dnipro.kremena.homeworks.list.ObjectList;
import ua.ithillel.dnipro.kremena.homeworks.list.impl.ArrayObjectList;
import ua.ithillel.dnipro.kremena.homeworks.services.PointService;

public class InMemoryPointService implements PointService {

    private ObjectList<Point> pointList = new ArrayObjectList<>();

    @Override
    public void addPoint(Point point) {
        pointList.add(point);
    }

    @Override
    public Point getPoint(int index) {
        return pointList.get(index);
    }

    @Override
    public ObjectList<Point> getAll() {
        return pointList;
    }

}