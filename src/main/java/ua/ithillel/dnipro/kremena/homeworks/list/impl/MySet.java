package ua.ithillel.dnipro.kremena.homeworks.list.impl;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.*;

public class MySet<E> implements Set<E> {

    private static final int DEFAULT_CAPACITY = 16;
    private Node<E>[] array;
    private int size;

    public MySet() {
        this(DEFAULT_CAPACITY);
    }

    public MySet(int capacity) {
        this.array = new Node[capacity];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        int i = calcIndex(o);
        if (array[i] == null) return false;
        for (Node<E> curr = array[i]; curr != null; curr = curr.getNext()) {
            if (curr.getValue().equals(o)) return true;
        }
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            private int index = getIndexBasket(0);
            private Node<E> curr = array[index];
            private Node<E> lastNode = getLastNode();
            @Override
            public boolean hasNext() {
                return curr != null;
            }
            @Override
            public E next() {
                if (curr == lastNode) {
                    curr = null;
                    return lastNode.getValue();
                }
                E value = curr.getValue();
                if (curr.getNext() != null) {
                    curr = curr.getNext();
                } else {
                    int nextIndex = getIndexBasket(++index);
                    if (nextIndex > 0) {
                        index = nextIndex;
                        curr = array[index];
                    }
                }
                return value;
            }

            private Node<E> getLastNode() {
                for (int index = array.length - 1; index >= 0; index--) {
                    if (array[index] != null) {
                        for (Node<E> node = array[index]; node != null; node = node.getNext()) {
                            if (node.getNext() == null) {
                                return node;
                            }
                        }
                    }
                }
                return null;
            }

            private int getIndexBasket(int index) {
                for (; index < array.length; index++) {
                    if (array[index] != null) {
                        return index;
                    }
                }
                return 0;
            }
        };
    }

    @Override
    public Object[] toArray() {
        return toArray(new Object[0]);
    }

    @Override
    public <T> T[] toArray(T[] a) {
        a = Arrays.copyOf(a, size);
        int index = 0;
        for (E e : this) {
            a[index++] = (T)e;
        }
        return a;
    }

    @Override
    public boolean add(E e) {
        int index = calcIndex(e);
        if (array[index] == null) {
            array[index] = new Node<E>().setValue(e);
            size++;
            return true;
        }
        for (Node<E> curr = array[index]; curr != null; curr = curr.getNext()) {
            if (curr.getValue().equals(e)) {
                return false;
            } else if (curr.getNext() == null) {
                curr.setNext(new Node<E>().setValue(e));
                size++;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean remove(Object o) {
        if (size() == 0 || o == null) return false;
        int index = calcIndex(o);

        if (array[index] == null) return false;

        if (array[index].getValue().equals(o) && array[index].getNext() == null) {
            array[index] = null;
            size--;
            return true;
        } else if (array[index].getValue().equals(o) && array[index].getNext() != null) {
            array[index] = array[index].getNext();
            size--;
            return true;
        }

        if (!(array[index].getValue().equals(o)) && array[index].getNext() != null) {
            for (Node<E> node = array[index]; node != null; node = node.getNext()) {
                if (node.getValue().equals(o) && node.getNext() != null) {
                    for (Node<E> prev = array[index];  prev != null ; prev = prev.getNext()) {
                        if (Objects.equals(prev.getNext(), node)) {
                            prev.setNext(node.getNext());
                            size--;
                            return true;
                        }
                    }
                }
                if (node.getValue().equals(o) && node.getNext() == null) {
                    for (Node<E> prev = array[index];  prev != null ; prev = prev.getNext()) {
                        if (Objects.equals(prev.getNext(), node)) {
                            prev.setNext(null);
                            size--;
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if(!contains(o)) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        int beforeSize = size;
        for (Object o : c) {
            add((E)o);
        }
        return beforeSize != size;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        int beforeSize = size;
        for (E e : this) {
            if (!c.contains(e)) {
                remove(e);
            }
        }
        return beforeSize != size;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        int beforeSize = size;
        for (Object o : c) {
            remove(o);
        }
        return beforeSize != size;
    }

    @Override
    public void clear() {
        for (Node<E> node : array) {
            node = null;
        }
        size = 0;
    }

    private int calcIndex(Object object) {
        return object.hashCode() % array.length;
    }

    @Getter @Setter
    @Accessors(chain = true)
    private static class Node<E> {
        private E value;
        private Node<E> next;
    }
}