package ua.ithillel.dnipro.kremena.homeworks.services.imp;

import ua.ithillel.dnipro.kremena.homeworks.entity.Circle;
import ua.ithillel.dnipro.kremena.homeworks.entity.Point;
import ua.ithillel.dnipro.kremena.homeworks.list.ObjectList;
import ua.ithillel.dnipro.kremena.homeworks.list.impl.ArrayObjectList;
import ua.ithillel.dnipro.kremena.homeworks.services.CircleService;

public class InMemoryCircleService implements CircleService {

    private Circle circle;

    @Override
    public void addCircle(Circle circle) {
        this.circle = circle;
    }

    @Override
    public Circle get() {
        return circle;
    }

    @Override
    public ObjectList<Point> getContainsPoint(Circle circle, ObjectList<Point> pointList) {
        ObjectList<Point> containsPointList = new ArrayObjectList<>();
        for (int index = 0; index < pointList.size(); index++) {
            if (circle.containsPoint(pointList.get(index))) {
                containsPointList.add(pointList.get(index));
            }
        }
        return containsPointList;
    }

    @Override
    public ObjectList<Point> getNotContainsPoint(Circle circle, ObjectList<Point> pointList) {
        ObjectList<Point> notContainsPointList = new ArrayObjectList<>();
        for (int index = 0; index < pointList.size(); index++) {
            if (!circle.containsPoint(pointList.get(index))) {
                notContainsPointList.add(pointList.get(index));
            }
        }
        return notContainsPointList;
    }
}