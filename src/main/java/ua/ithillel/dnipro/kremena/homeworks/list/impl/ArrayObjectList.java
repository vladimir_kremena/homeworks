package ua.ithillel.dnipro.kremena.homeworks.list.impl;

import ua.ithillel.dnipro.kremena.homeworks.list.ObjectList;

import java.util.Arrays;
import java.util.Iterator;
import java.util.function.BiFunction;
import java.util.function.Predicate;

public class ArrayObjectList<T> implements ObjectList<T> {

    private final static int DEFAULT_CAPACITY = 16;
    private final static double SCALE_FACTOR = 1.5;

    private Object[] objects;
    private int sizeList = 0;

    public ArrayObjectList() {
        this(DEFAULT_CAPACITY);
    }

    public ArrayObjectList(int capacity) {
        objects = new Object[capacity];
    }

    @Override
    public void add(T element) {
        if (sizeList >= objects.length) expand();
        objects[sizeList++] = element;
    }

    @Override
    public void addAll(ObjectList<T> list) {
        for (int index = 0; index < list.size(); index++) {
            add(list.get(index));
        }
    }

    @Override
    public T get(int index) {
        checkIndex(index);
        return (T)objects[index];
    }

    @Override
    public void set(int index, T element) {
        checkIndex(index);
        objects[index] = element;
    }

    @Override
    public int size() {
        return sizeList;
    }

    private void expand() {
        objects = Arrays.copyOf(objects, (int)(objects.length * SCALE_FACTOR + 1));
    }

    private void checkIndex(int index) {
        if (index < 0 || index > sizeList - 1) throw new IndexOutOfBoundsException(index);
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            private int index;

            @Override
            public boolean hasNext() {
                return index < sizeList;
            }

            @Override
            public T next() {
                checkIndex(index);
                return (T)objects[index++];
            }
        };
    }

    public Iterator<T> reversIteratorInnerClass() {
        return new IteratorInnerClass<>();
    }

    public Iterator<T> reversIteratorAnonymousClass() {
        return new Iterator<T>() {

            private int index = sizeList - 1;

            @Override
            public boolean hasNext() {
                return index >= 0;
            }

            @Override
            public T next() {
                return (T)objects[index--];
            }
        };
    }

    private class IteratorInnerClass<T> implements Iterator<T> {

        private int index = sizeList - 1;

        @Override
        public boolean hasNext() {
            return index >= 0;
        }

        @Override
        public T next() {
            return (T)objects[index--];
        }
    }
}