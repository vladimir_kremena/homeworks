package ua.ithillel.dnipro.kremena.homeworks.view.menu.imp;

import ua.ithillel.dnipro.kremena.homeworks.entity.Point;
import ua.ithillel.dnipro.kremena.homeworks.list.ObjectList;
import ua.ithillel.dnipro.kremena.homeworks.services.CircleService;
import ua.ithillel.dnipro.kremena.homeworks.services.PointService;
import ua.ithillel.dnipro.kremena.homeworks.view.menu.MenuItem;

public class NotContainsPointMenuItem implements MenuItem {

    private PointService pointService;
    private CircleService circleService;

    public NotContainsPointMenuItem(PointService pointService, CircleService circleService) {
        this.pointService = pointService;
        this.circleService = circleService;
    }

    @Override
    public String getName() {
        return "Показать точки вне окружности";
    }

    @Override
    public void execute() {
        ObjectList<Point> notContainsPointList = circleService.getNotContainsPoint(circleService.get(), pointService.getAll());
        if (notContainsPointList.size() == 0) {
            System.out.println("Вне окружности точки отсутствуют");
        } else {
            System.out.println("Список точек, которые лежат вне окружности : ");
            for (int index = 0; index < notContainsPointList.size(); index++) {
                System.out.printf("%2d - %s\n", index + 1, notContainsPointList.get(index));
            }
        }
    }
}