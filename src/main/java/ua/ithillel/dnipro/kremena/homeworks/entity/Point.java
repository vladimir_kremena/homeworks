package ua.ithillel.dnipro.kremena.homeworks.entity;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Point {

    private double xCoordinate;
    private double yCoordinate;

    public double distanceToPoint(Point point) {
        if (point == null) throw new IllegalArgumentException("Невозможно искать расстояние до null");
        return Math.sqrt((this.xCoordinate - point.getXCoordinate()) * (this.xCoordinate - point.getXCoordinate()) +
                (this.yCoordinate - point.getYCoordinate()) * (this.yCoordinate - point.getYCoordinate()));
    }

    @Override
    public String toString() {
        return  "Point [" +
                "xCoordinate = " + xCoordinate + " | " +
                "yCoordinate = " + yCoordinate +
                ']';
    }
}