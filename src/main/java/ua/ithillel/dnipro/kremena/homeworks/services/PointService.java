package ua.ithillel.dnipro.kremena.homeworks.services;

import ua.ithillel.dnipro.kremena.homeworks.entity.Point;
import ua.ithillel.dnipro.kremena.homeworks.list.ObjectList;

public interface PointService {

    void addPoint(Point point);

    Point getPoint(int index);

    ObjectList<Point> getAll();

}