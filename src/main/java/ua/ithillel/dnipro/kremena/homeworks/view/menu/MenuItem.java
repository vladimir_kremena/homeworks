package ua.ithillel.dnipro.kremena.homeworks.view.menu;

public interface MenuItem {

    String getName();

    void execute();

    default boolean isFinal() {
        return false;
    }

}