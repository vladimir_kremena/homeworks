package ua.ithillel.dnipro.kremena.homeworks.list;

import java.util.Iterator;
import java.util.function.BiFunction;
import java.util.function.Predicate;

public interface ObjectList<T> extends Iterable<T> {

    void add(T element);

    void addAll(ObjectList<T> list);

    T get(int index);

    void set(int index, T element);

    int size();

    Iterator<T> reversIteratorInnerClass();

    Iterator<T> reversIteratorAnonymousClass();

    default boolean every(Predicate<T> p) {
        int qtyValidElements = 0;
        for (T t : this) {
            if (p.test(t)) {
                qtyValidElements++;
            }
        }
        return qtyValidElements == size();
    }

    default boolean some(Predicate<T> p) {
        int qtyValidElements = 0;
        for (T t : this) {
            if (p.test(t)) {
                qtyValidElements++;
            }
        }
        return qtyValidElements >= 1;
    }

    default <R> R reduce(R iniValue, BiFunction<T, R, R> biF) {
        R result = iniValue;
        for(T t : this) {
            result = biF.apply(t, result);
        }
        return result;
    }
}