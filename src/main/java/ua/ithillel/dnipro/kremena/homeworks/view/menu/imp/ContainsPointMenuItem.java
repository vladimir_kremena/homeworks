package ua.ithillel.dnipro.kremena.homeworks.view.menu.imp;

import ua.ithillel.dnipro.kremena.homeworks.entity.Point;
import ua.ithillel.dnipro.kremena.homeworks.list.ObjectList;
import ua.ithillel.dnipro.kremena.homeworks.services.CircleService;
import ua.ithillel.dnipro.kremena.homeworks.services.PointService;
import ua.ithillel.dnipro.kremena.homeworks.view.menu.MenuItem;

public class ContainsPointMenuItem implements MenuItem {

    private PointService pointService;
    private CircleService circleService;

    public ContainsPointMenuItem(PointService pointService, CircleService circleService) {
        this.pointService = pointService;
        this.circleService = circleService;
    }

    @Override
    public String getName() {
        return "Показать точки лежащие в окружности";
    }

    @Override
    public void execute() {
        ObjectList<Point> containsPointList = circleService.getContainsPoint(circleService.get(), pointService.getAll());
        if (containsPointList.size() == 0) {
            System.out.println("В пределеах окружности точки отсутствуют");
        } else {
            System.out.println("Список точек, которые лежат в окружности : ");
            for (int index = 0; index < containsPointList.size(); index++) {
                System.out.printf("%2d - %s\n", index + 1, containsPointList.get(index));
            }
        }
    }
}