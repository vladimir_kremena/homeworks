package ua.ithillel.dnipro.kremena.homeworks.services;

import ua.ithillel.dnipro.kremena.homeworks.entity.Circle;
import ua.ithillel.dnipro.kremena.homeworks.entity.Point;
import ua.ithillel.dnipro.kremena.homeworks.list.ObjectList;

public interface CircleService {

    void addCircle(Circle circle);

    Circle get();

    ObjectList<Point> getContainsPoint(Circle circle, ObjectList<Point> pointList);

    ObjectList<Point> getNotContainsPoint(Circle circle, ObjectList<Point> pointList);
}