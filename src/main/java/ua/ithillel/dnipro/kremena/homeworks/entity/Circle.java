package ua.ithillel.dnipro.kremena.homeworks.entity;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Circle {

    private Point center;
    private double radius;

    public boolean containsPoint(Point point) {
        double distance = center.distanceToPoint(point);
        return distance <= radius;
    }

    @Override
    public String toString() {
        return  "Circle [" +
                "center = " + center + " | " +
                "radius = " + radius +
                ']';
    }
}