package ua.ithillel.dnipro.kremena.homeworks.view.menu.imp;

import ua.ithillel.dnipro.kremena.homeworks.services.CircleService;
import ua.ithillel.dnipro.kremena.homeworks.view.menu.MenuItem;

public class ShowCircleMenuItem implements MenuItem {

    private CircleService circleService;

    public ShowCircleMenuItem(CircleService circleService) {
        this.circleService = circleService;
    }

    @Override
    public String getName() {
        return "Показать окружность";
    }

    @Override
    public void execute() {
        if (circleService.get() == null) {
            System.out.println("Окружность еще не создана");
        } else {
            System.out.println(circleService.get());
        }
    }
}